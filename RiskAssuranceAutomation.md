# Context

The Cyber Assurance team, aka GRC, perform risk assessments and information classification (CIA) assessments daily for both operational activities and for projects at City. Primary tools that are used are two types of spreadsheets that are pulled and filled for each engagement activity. These are then stored in our EDMS, Objective, and a companion GRC register spreadsheet is also filled. This provides assurance to the recipients of the outputs that risks have been appropriately considered and provides any mitigations that are required.

## Problems

There are many opportunities for improvements to our risk assurance process problems. 

* approx. 2-5mins is spent on accessing, opening excel, and downloading a spreadsheet from Objective for assessment
* approx. 2-5mins is spent on opening excel, uploading and categorising a spreadsheet into Objective
* approx. 2-5mins is spent on accessing, loading excel and saving the GRC register in Objective
* we are obligated to store our assessments _only_ in Objective, and are dissuaded from using anything else (e.g. own app, sharepoint, etc.)
* each risk assessment is bespoke in the considered controls, resulting in inconsistent declaration of required controls and mitigation controls
* the information gathering requirements are unique to each security assessment and are not consistent. E.g. IDEA, Project Number, information source, etc. As an assessor this results in administrative overhead in finding and identifying what is the relevant information for the scope of assessment.
* recipients or customers of risk assessments/assurance artifacts also have to go through a similar access and authorisation process in order to see the output of an assessment
* there's no easy way to report on assurance activities that are performed
* there's no way to perform analytics of assurance activities performed, restricting the ability to prioritise work or identify difficult areas of business
* some elements of information classification could be self serviced but excel spreadsheets in Objective are a barrier to bringing this into effect.
* CIA assessments typically take a minimum of 30mins and up to 1 hour. Whilst valuable to have engagement with customers, it's not an engaging discussion for them.
* cyber security standard control sets are not codified and are not referencable, resulting in time wasted reviewing PDF standards and then extrapolating requirements out of these documents


## Opportunity

* standardise control sets that are used for risk assessments
* analytics allow for proactively informed conversations with customers




